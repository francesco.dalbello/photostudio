﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoStudioV2
{
    class DataEditor
    {
        public static Bitmap editWithFilter(Bitmap image, Filters.effect thisEffect, object effectParameter)
        {
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                    image.SetPixel(x, y, thisEffect(image.GetPixel(x, y), effectParameter ));
            }
            return image;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoStudioV2
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load_1(object sender, EventArgs e)
        {
            
            
        }

        private void MainForm_Load_2(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //image loader
            OpenFileDialog loader = new OpenFileDialog();
            loader.Title = "Open Image";
            loader.Filter = "Image File|*.png|Image File|*.jpg|Image File|*.jpeg|Image File|*.bmp";
            loader.Title = "Select image";
            loader.RestoreDirectory = true;

            if (loader.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MyImage _img = new MyImage();
                _img.Path = loader.FileName;
                _img.Image = new Bitmap(_img.Path);
                ImageFormEditor formContainer = new ImageFormEditor(_img);
                formContainer.MdiParent = this;
                formContainer.Text = _img.Nome;
                formContainer.Show();

            }
        }



        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

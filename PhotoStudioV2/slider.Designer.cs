﻿namespace PhotoStudioV2
{
    partial class Slider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackFilter = new System.Windows.Forms.TrackBar();
            this.nCounter = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trackFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCounter)).BeginInit();
            this.SuspendLayout();
            // 
            // trackFilter
            // 
            this.trackFilter.Location = new System.Drawing.Point(12, 36);
            this.trackFilter.Maximum = 100;
            this.trackFilter.Minimum = -100;
            this.trackFilter.Name = "trackFilter";
            this.trackFilter.Size = new System.Drawing.Size(286, 45);
            this.trackFilter.TabIndex = 0;
            this.trackFilter.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // nCounter
            // 
            this.nCounter.Location = new System.Drawing.Point(316, 36);
            this.nCounter.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nCounter.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nCounter.Name = "nCounter";
            this.nCounter.Size = new System.Drawing.Size(37, 20);
            this.nCounter.TabIndex = 1;
            this.nCounter.ValueChanged += new System.EventHandler(this.nCounter_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(264, 93);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Applica filtro";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Slider
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 128);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nCounter);
            this.Controls.Add(this.trackFilter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Slider";
            this.Text = "slider";
            this.Load += new System.EventHandler(this.slider_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCounter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackFilter;
        private System.Windows.Forms.NumericUpDown nCounter;
        private System.Windows.Forms.Button button1;
    }
}
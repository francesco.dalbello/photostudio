﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoStudioV2
{
    public class MyImage
    {

        public Bitmap Image { get; set; }
        public string Nome { get => System.IO.Path.GetFileNameWithoutExtension(Path); }
        public string Path { get; set; }

        public MyImage()
        {
            Image = null;
            Path = "";
        }

        public MyImage(Bitmap _image, string _path)
        {
            Image = _image;
            Path = _path;

        }


    }
}

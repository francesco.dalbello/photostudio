﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoStudioV2
{
    public partial class Slider : Form
    {
        public ImageFormEditor imgFromEditor { get; set; }
        public Filters.effect effect { get; set; }
        public Slider()
        {
            InitializeComponent();
        }

        private void slider_Load(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            nCounter.Value = trackFilter.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float m = trackFilter.Value;
            DataEditor.editWithFilter(imgFromEditor.myImage.Image, Filters.brightnessEffect, (object)m);
            imgFromEditor.Refresh(); //refresh form
        }

        private void nCounter_ValueChanged(object sender, EventArgs e)
        {
            trackFilter.Value = (int)nCounter.Value;
        }
    }
}

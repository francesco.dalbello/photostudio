﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoStudioV2
{
    public partial class ImageFormEditor : Form
    {
        public MyImage myImage { get; set; }

        private Stack<Image> stackBefore = new Stack<Image>(); //stack for "undo" command
        private Stack<Image> stackAfter = new Stack<Image>();  //stack for "redo" command

        public ImageFormEditor(MyImage _img)
        {
            myImage = _img;
            InitializeComponent();
            setWidth();
            setPicture();
        }

        public void ImageFormEditor_Load(object sender, EventArgs e)
        {
        }

        //set width of picture as width of image
        public void setWidth()
        {
            int newPictBoxWidth = ((this.Height - 64) * myImage.Image.Width) / myImage.Image.Height;
            this.Size = new Size(newPictBoxWidth + 16, this.Height);
            
        }
        
        //set picture to the my picturebox
        public void setPicture()
        {
            pictureImg.Image = myImage.Image;
        }

        //save a copy of image before edit.

        private void pushStackBefore()
        {
            stackBefore.Push((Image)pictureImg.Image.Clone());
        }
        private void pushStackAfter()
        {
            stackAfter.Push((Image)pictureImg.Image.Clone());
        }
        private void ImageFormEditor_Resize(object sender, EventArgs e)
        {
            //resize picturebox
            setWidth();
        }

        //
        // Effects zone start
        //
        private void negativeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pushStackBefore();
            DataEditor.editWithFilter(myImage.Image, Filters.negativeEffect, null);
            this.Refresh(); //refresh form content
            pushStackAfter();
        }

        private void whiteAndBlackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pushStackBefore();
            DataEditor.editWithFilter(myImage.Image, Filters.greyEffect, null);
            this.Refresh(); //refresh form content
            pushStackAfter();
        }

        private void brightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pushStackBefore();
            Slider firstSlider = new Slider();  //slider form
            firstSlider.imgFromEditor = this;
            firstSlider.effect = Filters.brightnessEffect;
            firstSlider.Show();
            pushStackAfter();
        }

        private void blackWhiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pushStackBefore();
            DataEditor.editWithFilter(myImage.Image, Filters.blackwhiteEffect, null);
            this.Refresh(); //refresh form content
            pushStackAfter();
        }

        private void sepiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pushStackBefore();
            DataEditor.editWithFilter(myImage.Image, Filters.sepiaEffect, null);
            this.Refresh(); //refresh form content
            pushStackAfter();
        }
        //
        // Effects zone end
        ///

        //User Commands
        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog saver = new SaveFileDialog();
            saver.Filter = "Image File|*.png|Image File|*.jpg|Image File|*.jpeg|Image File|*.bmp";
            saver.Title = "Save as";
            saver.RestoreDirectory = true;
            DialogResult dialogResult = saver.ShowDialog();
            if (saver.FileName != "" && dialogResult == DialogResult.OK)
            {
                myImage.Image.Save(saver.FileName);
                myImage.Path = saver.FileName;
                this.Refresh();
            }
        }

        private void undoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (stackBefore.Count != 0)
            {
                pictureImg.Image = stackBefore.Pop();
               // pushStackAfter();
                setWidth();
                this.Refresh();            
}
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (stackAfter.Count != 0)
            {
                pictureImg.Image = stackAfter.Pop();
                //pushStackBefore();
                setWidth();
                this.Refresh();
            }

        }
    }
}

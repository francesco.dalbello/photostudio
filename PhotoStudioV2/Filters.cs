﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoStudioV2
{
    public static class Filters
    {
        public delegate Color effect(Color input, object effectParameter);

        public static Color negativeEffect(Color input, object _)
        {
            return Color.FromArgb(input.A, (255 - input.R), (255 - input.G), (255 - input.B));
        }

        public static Color greyEffect(Color input, object _)
        {
            int var = (input.R + input.G + input.B) / 3;
            return Color.FromArgb(var, var, var);
        }

        public static Color brightnessEffect(Color input, object m)
        {
            float red = input.R;
            float green = input.G;
            float blue = input.B;

            red = red + (float)m;
            green = green + (float)m;
            blue = blue + (float)m;

            //red
            if (red > 255)
                red = 255;
            else if (red < 0)
                red = 0;

            //green
            if (green > 255)
                green = 255;
            else if (green < 0)
                green = 0;

            //blue
            if (blue > 255)
                blue = 255;
            else if (blue < 0)
                blue = 0;


            return Color.FromArgb((int)red, (int)green, (int)blue); 
        }

        public static Color blackwhiteEffect(Color input, object _)
        {
            float red = input.R;
            float green = input.G;
            float blue = input.B;


            if (red > 128)
            {
                red = 255;
                green = 255;
                blue = 255;
            }
            if (red < 128)
            {
                red = 0;
                green = 0;
                blue = 0;
            }


            return Color.FromArgb((int)red, (int)green, (int)blue);
        }
        public static Color sepiaEffect(Color input, object _)
        {
            int r, g, b;
            float red = input.R;
            float green = input.G;
            float blue = input.B;

            int tr = (int)(0.393 * red) + (int)(0.769 * green) + (int)(0.189 * blue);
            int tg = (int)(0.349 * red) + (int)(0.686 * green) + (int)(0.168 * blue);
            int tb = (int)(0.272 * red) + (int)(0.534 * green) + (int)(0.131 * blue);

            if (tr > 255)
                r = 255;
            else r = tr;

            if (tg > 255)
                g = 255;
            else g = tg;
            if (tb > 255)
                b = 255;
            else b = tb;          

            return Color.FromArgb((int)r, (int)g, (int)b);
        }


    }
}
